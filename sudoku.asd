(asdf:defsystem :sudoku
    :depends-on (:mcclim)
  :components
  ((:file "packages")
   (:file "sudoku-gui")
   (:file "sudoku-algorithm")))
